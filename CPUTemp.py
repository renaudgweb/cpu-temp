#!/usr/bin/env python3

import tm1637
from time import sleep

# Initialisation de l'afficheur
tm = tm1637.TM1637(clk=23, dio=24)
tm.brightness(2)

# Fonction qui recupere et renvoie la temperature en float
def recup_temp(emplacement):
    fich_temp = open(emplacement)
    contenu_fich = fich_temp.read()
    fich_temp.close()
    temperature_data = contenu_fich
    temperature = float(temperature_data) / 1000
    return temperature

# Affiche la temperature recuperee par la fonction recup_temp (arrondie)
def afficher_temp(emplacement_fich):
    temperature = recup_temp(emplacement_fich)
    tm.temperature(int(round(temperature)))

# Boucle inifinie appelant la fonction afficher_temp
while True:
    afficher_temp("/sys/class/thermal/thermal_zone0/temp")
    sleep(2)
